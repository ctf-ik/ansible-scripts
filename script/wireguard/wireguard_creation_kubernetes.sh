#!/bin/bash
# JULIEN - 23/11/2021 - Creation script permettant la genereation de la conf du serveur et des clients sur wireguard
# JULIEN - 26/11/2021 - Ajout du stockage des clees client PSK et PRIV pour eviter de les regenerer a chaque passe du script
# JULIEN - 30/11/2021 - Refonte du script pour etre adapte a kubernetes. 
# JULIEN - 17/05/2022 - Modification de la gestion des ip client

# Chemin vers le fichier de variable du role de deploiement de wireguard
var_file="../ansible-scripts/role/deploy_wireguard_container_k3s/vars/main.yml"

# Chemin vers le dossier qui contient ce script 
script_path="../ansible-scripts/script/wireguard"

# Chemin vers le dossier qui contient le secret des clients 
client_secret="$script_path/client_secret"

# Chemin vers le dossier qui contient le secret du serveur
server_secret="$script_path/server_secret"

# Master Server IP
master_ip=$2


mkdir -p $client_secret
mkdir -p $server_secret

# Test de l'existence du fichier qui fait la correspondance entre le nom des equipes et le port du VPN 
if test -f "$script_path/ekip.csv"; then
    true
else
    touch $script_path/ekip.csv    
fi

# Le premier port utilise sera le port 51823
i=51823
# While qui permet de recuperer le nom des equipes et de leurs assigner un port 
while IFS=";" read -r equipe pseudo 
    do
        if  grep -q "$equipe" "$script_path/ekip.csv" ; then
            true
        else
            ((i=i+1))
            echo "$equipe;$i"
        fi
done < <(tail -n +2 $1) >> $script_path/ekip.csv

# Generation d'une clee privee par equipe
while IFS=";" read -r equipe port
    do
        # Generation de la clee privee du serveur wireguard
        # Ici la clee devra etre stockee dans VAULT, pour eviter de regenerer la clee prive du serveur a chaque regeneration des acces client
        file=$server_secret/server_key_priv_$equipe
        if test -f "$file"; then
            true
        else
            wg genkey > $server_secret/server_key_priv_$equipe
        fi

done < <(cat $script_path/ekip.csv)


echo "---" > $var_file
echo " " >> $var_file

echo "server_priv:" >> $var_file
while IFS=";" read -r equipe port
    do
    echo "- { server_key_priv: $(cat $server_secret/server_key_priv_$equipe ), equipe: $(echo $equipe), port: $(echo $port) }" 2>/dev/null
done < <(cat $script_path/ekip.csv) >> $var_file

echo "  " >> $var_file
echo "copie_conf_client:" >> $var_file

ip=2
ip2=0
while IFS=";" read -r equipe pseudo
    do
   

    if test -f "$client_secret/$pseudo.priv"; then
        true
    else
        #Generation de la clee prive de l'utilisateur
        wg genkey > $client_secret/$pseudo.priv 2>/dev/null
    fi

    # Generation de la clee public du serveur
    wg pubkey < $server_secret/server_key_priv_$equipe > server_key_public_$equipe 2>/dev/null
    # Generation de la clee public du client
    wg pubkey < $client_secret/$pseudo.priv > client_key_public 2>/dev/null

    if test -f "$client_secret/$pseudo.psk"; then
        true
    else
        #Generation de la PSK
        wg genpsk > $client_secret/$pseudo.psk 2>/dev/null
    fi

    if test -f "$client_secret/$pseudo.ip"; then
        true
    else 
        if [[ $ip -lt 255 ]]; then
            echo "10.6.$ip2.$ip/32" > $client_secret/$pseudo.ip 2>/dev/null
        else
            ((ip2=ip2+1))
            ip=2
            echo "10.6.$ip2.$ip/32" > $client_secret/$pseudo.ip 2>/dev/null
        fi 
    fi
    ((ip=ip+1))
    while IFS=";" read -r equipe2 port
    do 
        if [ "$equipe" = "$equipe2" ]; then 
            echo "  "
            echo "- { src: client_conf.j2, dst: '{{ role_path }}/../../../cli/user_conf/$pseudo.conf', owner: '{{ ansible_user_id }}', group: '{{ ansible_user_id }}', mode: '0700', server_key_priv: $(cat $server_secret/server_key_priv_$equipe ) , server_key_public: $(cat server_key_public_$equipe), client_key_priv: $(cat $client_secret/$pseudo.priv), client_key_public: $(cat client_key_public), client_psk: $(cat $client_secret/$pseudo.psk), adresse_ip: $(cat $client_secret/$pseudo.ip) ,client_info: $equipe=$pseudo, port: $(echo $port), master_ip: $(echo $master_ip) }"
            rm server_key_public_$equipe
        fi
      
    done < $script_path/ekip.csv >> $var_file

done < <(tail -n +2 $1)


# Suppression des fichiers inutiles
rm client_key_public
# A remettre des que VAULT operationnel 
#rm server_key_priv


