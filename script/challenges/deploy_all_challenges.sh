#!/bin/bash
#
# Olivier - 01/12/2021 Créaton du script
#
#
##############VARIABLES#####################
DIR_MAIN_ROLE_ANSIBLE=../ansible-scripts
DIR_SCRIPTS=../ansible-scripts/script
FILE_INVENTORIES=../ansible-scripts/inventories
DIR_CHALLENGE=$DIR_MAIN_ROLE_ANSIBLE/script/challenges
EXTRA_VARS=$DIR_CHALLENGE/vars_challenge.yml
fic_csv=$1
challenge_namespace=challenge
###########################################


echo "------------Génération du fichier de variable ----------"
bash $DIR_CHALLENGE/get_csv_challenge.sh $fic_csv $challenge_namespace

if [[ $? == 0 ]];
then
    echo "------------Déploiement des challenges-----------"
    ansible-playbook -i $FILE_INVENTORIES $DIR_MAIN_ROLE_ANSIBLE/main_deploy_challenge.yml --extra-vars "@$EXTRA_VARS" --extra-vars "namespace_challenge=$challenge_namespace"
    echo "------------Création des politiques réseaux-----------"
    ansible-playbook -i $FILE_INVENTORIES $DIR_MAIN_ROLE_ANSIBLE/main_create_network_policies.yml --extra-vars "@$EXTRA_VARS" --extra-vars "namespace_challenge=$challenge_namespace"
else
    echo "Erreur dans la génération du fichier de variable"
fi
