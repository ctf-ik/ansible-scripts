#!/bin/bash
#
#
# Script permettant l'export des info des challs vers ctfd 


VAR_FILE=$1
IMPORTABLE_FILE=$(pwd)/challenge_for_ctfd.csv
count=2

error_function()
{
    echo "Un argument requis (csv des challenges)"
    return 127
}

echo "id,name,description,connection_info,max_attempts,value,flags,category,type,state,requirements" > $IMPORTABLE_FILE 2>/dev/null
while IFS="," read -r name type protocole port port_container  image emplacement_container data fic_name_config flag_value points_number env_variables
do 

   echo "$count,$name,description a modifier,$name-[Nom de votre equipe].challenge.svc.cluster.local:$port,0,$points_number,$flag_value,$type,standard,visible," >> $IMPORTABLE_FILE 2>/dev/null
   ((count=count+1))

done < <(tail -n+2 $VAR_FILE)


