#!/bin/bash
#
#
#
DIR_SCRIPTS=../ansible-scripts/script
VAR_FILE=$DIR_SCRIPTS/challenges/vars_challenge.yml
VAR_TEAM=$DIR_SCRIPTS/wireguard/ekip.csv
namespace=$2

error_function()
{
    echo "Un argument requis (csv des challenges)"
    return 127
}


if [[ "$#" == 2 ]];
then
    echo "---" > $VAR_FILE
    echo "challenge :" >> $VAR_FILE
    while IFS=";" read -r equipe joueur pseudo ip
    do
        while IFS="," read -r name type protocole port port_container  image emplacement_container data fic_name_config flag_value points_number env_variables
        do 
            if [[ ! -z "$env_variables" ]];then
                echo "- { number: 1, namespace: \"$namespace\", equipe: $equipe ,dest: \"/tmp/challenge-$name-$type-$equipe.yml\", name: \"$name\", type: $type, protocole: $protocole, port: $port, port_container: $port_container, image: \"$image\", dir_container: \"$emplacement_container\", data: \"$data\", fic_name_config: \"$fic_name_config\",env_var: \"True\" }" >> $VAR_FILE
	    else
            echo "- { number: 1, namespace: \"$namespace\", equipe: $equipe ,dest: \"/tmp/challenge-$name-$type-$equipe.yml\", name: \"$name\", type: $type, protocole: $protocole, port: $port, port_container: $port_container, image: \"$image\", dir_container: \"$emplacement_container\", data: \"$data\", fic_name_config: \"$fic_name_config\",env_var: \"False\" }" >> $VAR_FILE
	    fi

        done < <(tail -n+2 $1)
    done <  $VAR_TEAM
else
    error_function
fi

echo "      " >> $VAR_FILE

if [[ "$#" == 2 ]];
then
    echo "env_variables :" >> $VAR_FILE
        while IFS="," read -r name type protocole port port_container  image emplacement_container data fic_name_config flag_value points_number env_variables
        do
	    if [[ ! -z "$env_variables" ]];then
                readarray -t arrayv < <(tr ';' '\n' <<< "$env_variables")
	        for e in "${arrayv[@]}";do
	            readarray -t variables < <(tr '=' '\n' <<< "$e")
                    echo "- { var_name: \"${variables[0]}\", var_value: \"${variables[1]}\", name: \"$name\" }"  >> $VAR_FILE
                done
	    fi
        done < <(tail -n+2 $1)
else
    error_function
fi


