#!/bin/bash

# JULIEN - 05/01/2022 - Script qui permet de configurer les machines qui vont heberger le cluster k3s


while IFS="," read -r ip user password
do
    echo $ip
    sshpass -p $password ssh $user@$ip 'sudo adduser ctf-ik --home /home/ctf-ik --shell /bin/bash --disabled-password'  </dev/null 2>/dev/null
    sshpass -p $password ssh $user@$ip 'echo "%ctf-ik  ALL=(ALL)       NOPASSWD:ALL" > /tmp/ctf-ik' </dev/null 2>/dev/null
    sshpass -p $password ssh $user@$ip 'sudo mv /tmp/ctf-ik /etc/sudoers.d/ctf-ik' </dev/null 2>/dev/null
    sshpass -p $password ssh $user@$ip 'sudo chown root: /etc/sudoers.d/ctf-ik' </dev/null 2>/dev/null
    sshpass -p $password scp ~/.ssh/id_ecdsa.pub $user@$ip:/tmp/ </dev/null 2>/dev/null
    sshpass -p $password ssh $user@$ip 'sudo mkdir /home/ctf-ik/.ssh/' </dev/null 2>/dev/null
    sshpass -p $password ssh $user@$ip 'sudo mv /tmp/id_ecdsa.pub /home/ctf-ik/.ssh/authorized_keys' </dev/null 2>/dev/null
    sshpass -p $password ssh $user@$ip 'sudo chown -R ctf-ik: /home/ctf-ik/' </dev/null 2>/dev/null
done < <(tail -n +2 $1)
